#!/bin/bash
echo "Build started on $(date)"
set -e
npm install

if [ -z "$CI_COMMIT_REF_NAME" ]; then
    GIT_BRANCH=$(git symbolic-ref --short -q HEAD)
else
    GIT_BRANCH=$CI_COMMIT_REF_NAME
fi

    ##############Android build#################
    echo "Android Build started on $(date)"
    pushd .
    cd android/
    echo "---------- local.properties -----------"
    touch local.properties
    cat local.properties || true
    echo "---------- end local.properties -----------"
    gradle --refresh-dependencies \
           assembleRelease \
           uploadToHockeyApp
  
